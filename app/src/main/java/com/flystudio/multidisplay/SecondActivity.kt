package com.flystudio.multidisplay

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.flystudio.multidisplay.databinding.ActivitySecondBinding

class SecondActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySecondBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySecondBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btShowToast.setOnClickListener {
            Toast.makeText(this@SecondActivity,"This is Toast", Toast.LENGTH_LONG).show()
        }
        findViewById<View>(R.id.btClose).setOnClickListener { finish() }
    }
}