package com.flystudio.multidisplay

import android.app.Presentation
import android.content.Context
import android.os.Bundle
import android.view.Display
import android.view.View
import android.widget.Toast

class MyPresentation(outerContext: Context, display: Display) :
    Presentation(outerContext, display) {

    /**
     * Bundle参数类型必须加上?，否则会异常
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_presentation)

        initView()
    }

    private fun initView() {
        findViewById<View>(R.id.btShowToast).setOnClickListener {
            Toast.makeText(context,"This is Toast", Toast.LENGTH_LONG).show()
        }
        findViewById<View>(R.id.btClose).setOnClickListener { dismiss() }
    }
}
