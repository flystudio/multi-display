package com.flystudio.multidisplay

import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.hardware.display.DisplayManager
import android.media.MediaRouter
import android.os.Bundle
import android.util.Log
import android.view.Display
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.flystudio.multidisplay.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    companion object{
        private const val TAG = "MainActivity"
    }
    private lateinit var binding: ActivityMainBinding
    private lateinit var mDisplayManager:DisplayManager
    private lateinit var displays:Array<Display>
    private var myPresentation: MyPresentation? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mDisplayManager = getSystemService(DISPLAY_SERVICE) as DisplayManager
        displays = mDisplayManager.displays

        initView()

        initEvent()
    }

     private fun initView() {
        val stringBuilder: StringBuilder = StringBuilder("当前设备支持的屏幕:\n")
        for (display in displays) {
            stringBuilder.append(
                "name:${display.name},displayId:${display.displayId},rotation:${display.rotation}," +
                        "size:${display.mode.physicalWidth}x${display.mode.physicalHeight}\n"
            )
        }
        binding.tvInfo.text = stringBuilder.toString()
    }

    private fun initEvent() {
        binding.btOpenActivity.setOnClickListener {
            //移除之前打开的Presentation
            if (myPresentation != null) {
                myPresentation!!.dismiss()
                myPresentation = null
            }

            val secondIntent = Intent(this, SecondActivity::class.java)
            if (displays.size > 1) {
                //这句很重要，不添加则无法推送到副屏
                secondIntent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                val options = ActivityOptions.makeBasic()
                //显示到第2块屏，id不一定是0、1、2递增的
                options.launchDisplayId = displays[1].displayId
                startActivity(secondIntent, options.toBundle())
            } else {
                startActivity(secondIntent)
            }
        }
        binding.btMediaRouter.setOnClickListener {
            //移除之前打开的Presentation
            if (myPresentation != null) {
                myPresentation!!.dismiss()
                myPresentation = null
            }

            val mediaRouter = getSystemService(MEDIA_ROUTER_SERVICE) as MediaRouter
            val route = mediaRouter.getSelectedRoute(MediaRouter.ROUTE_TYPE_LIVE_VIDEO)
            var presentationDisplay: Display? = null
            if (route != null) {
                presentationDisplay = route.presentationDisplay
                if (presentationDisplay != null) {
                    try {
                        myPresentation = MyPresentation(this@MainActivity, presentationDisplay)
                        myPresentation?.show()
                    } catch (ex: Exception) {
                        Log.w(TAG, "Couldn't show presentation!", ex)
                        Toast.makeText(this@MainActivity, ex.message, Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            }
            if (route == null || presentationDisplay == null) {
                Toast.makeText(this@MainActivity, "不支持Presentation分屏", Toast.LENGTH_SHORT)
                    .show()
            }
        }
        binding.btDisplayManager.setOnClickListener {
            //移除之前打开的Presentation
            if (myPresentation != null) {
                myPresentation!!.dismiss()
                myPresentation = null
            }

            //只获取适用于PRESENTATION使用的显示屏(该方式测试无效，获取的size是0)
            //val displays = mDisplayManager.getDisplays(DisplayManager.DISPLAY_CATEGORY_PRESENTATION)
            val displays = mDisplayManager.displays
            if (displays.size > 1) {
                try {
                    myPresentation = MyPresentation(this@MainActivity, displays[1])
                    myPresentation?.show()
                } catch (ex: Exception) {
                    Log.w(TAG, "Couldn't show presentation!", ex)
                    Toast.makeText(this@MainActivity, ex.message, Toast.LENGTH_SHORT)
                        .show()
                }
            } else {
                Toast.makeText(this@MainActivity, "不支持Presentation分屏", Toast.LENGTH_SHORT)
                    .show()
            }
        }
        binding.btClosePresentation.setOnClickListener {
            if (myPresentation != null) {
                myPresentation!!.dismiss()
                myPresentation = null
            } else {
                Toast.makeText(this@MainActivity, "Presentation is null", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }
}